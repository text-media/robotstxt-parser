<?php

namespace TextMedia\RobotsTxtParser\Tests;

use TextMedia\RobotsTxtParser\RobotsAgent;
use TextMedia\RobotsTxtParser\RobotsTxt;

use PHPUnit\Framework\TestCase;

/**
 * Тестирование парсера robots.txt.
 */
class ParserTest extends TestCase
{
    /** @var \TextMedia\RobotsTxtParser\RobotsAgent */
    protected $textAgent;

    /** @var \TextMedia\RobotsTxtParser\RobotsAgent[] */
    protected $agents;

    /** @var \TextMedia\RobotsTxtParser\RobotsTxt */
    protected $robots;

    /** Создание тестируемых объектов перед тестами. */
    public function setUp()
    {
        $this->textAgent = new RobotsAgent('text');
        $this->agents = [$this->textAgent, new RobotsAgent];
        $this->robots = new RobotsTxt("
            # Коментарии должны игнорирваться.
            User-Agent: Yandex # Этот блок должен отсутствовать в парсере.
            Disallow: /

            User-Agent: *
            Disallow: # Запрещает индексацию всех страниц.

            User-Agent: Text  # Этот блок должен присутствовать, несмотря на разницу в регистрах имен.
            Allow:            # Инструкция должна быть проигнорирована.
            Disallow: /test   # Запрещает всё, начинающееся с '/test'.
            Disallow: qwerty  # Запрещает всё, содержащее 'qwerty'.
            Allow:    /asdfgh # Разрешает всё, начинающееся с 'asdfgh', и должно перекрывать следующее правило.
            Disallow: asdfgh$ # Запрещает всё, заканчивающееся на 'asdfgh'.
            Disallow: *.js$   # Запрещает все 'js'-файлы.
            Disallow: /*?     # Запрещает страницы с GET-запросами.
        ", $this->agents);
    }

    /**
     * Тест разбора robots.txt.
     */
    public function testParser()
    {
        $count = count($this->agents);
        $this->assertCount($count, $this->robots->getAgents(), "Количество блоков должно быть раным {$count}");

        $agents = array_map(function (RobotsAgent $agent): string {
            return $agent->getName(true);
        }, $this->agents);
        $allowAll = $disallowAll = null;
        foreach ($this->robots->getBlocks() as $block) {
            $agent = $block->getAgent()->getName(true);
            $this->assertNotEmpty($agent, "Имя блока не должно быть пустым ({$block->getAgent()})");
            $this->assertContains($agent, $agents, "Не должно быть лишних блоков ({$agent})");

            foreach ($block->getRules() as $rule) {
                $this->assertNotEmpty($rule->getMask(), "Правило не должно быть пустым ({$rule})");
                if ($rule->getMask() === '/') {
                    if ($rule->getAllows()) {
                        $allowAll = $rule;
                    } else {
                        $disallowAll = $rule;
                    }
                }
            }
        }

        $this->assertNull($allowAll, 'Инструкция "allow:" должна игнорироваться');
        $this->assertNotNull($disallowAll, 'Инструкция "disallow:" не должна игнорироваться');
    }

    /**
     * Тест проверки URL.
     *
     * @param string  $url       URL для проверки.
     * @param boolean $allAgents Искать по всем агентам или только для "text".
     * @param boolean $allows    Должна ли ссылка быть разрешена.
     * @param string  $message   Сообщение/описание.
     *
     * @dataProvider matchesProvider
     */
    public function testMatches(string $url, bool $allAgents, bool $allows, string $message)
    {
        $method = $allows ? 'allows' : 'disallows';
        $agents = $allAgents ? $this->agents : [$this->textAgent];
        $this->assertTrue(call_user_func([$this->robots, $method], $url, $agents), $message);
    }

    /**
     * Поставщик тестов для testMatches().
     *
     * @return array
     */
    public function matchesProvider(): array
    {
        return [
            // URL                        AllAgents? Allows? Message
            ['http://domain.tld/test/12', true,      false,  '/test/12 должно быть запрещено блоком text'],
            ['http://domain.tld/12/test', true,      false,  '/12/test должно быть запрещено блоком *'],
            ['http://domain.tld/12/test', false,     true,   '/12/test должно быть разрешено блоком text'],

            ['/xasdfgh',                  false,     false,  '/xasdfgh должно быть запрещено'],
            ['/asdfgh',                   true,      true,   '/asdfgh должно быть разрешено'],

            ['/any?query',                false,     false,  '/any?query должно быть запрещено'],
            ['/asdfgh?query',             false,     true,   '/asdfgh?query должно быть разрешено'],

            ['/qwerty',                   false,     false,  'URL, содержащие qwerty должны быть запрещены, где бы эта строка не стояла'],
            ['/any#qwerty/xxx',           false,     false,  'URL, содержащие qwerty должны быть запрещены, где бы эта строка не стояла'],

            ['/any/script.js',            false,     false,  'JS-файлы должны быть запрещены'],
            ['/another/text.js.txt',      false,     true,   'Имена типа .jsXXX - должны быть разрешены'],
        ];
    }
}
