<?php

namespace TextMedia\RobotsTxtParser;

/**
 * Правило robots.txt.
 */
class RobotsRule extends AbstractRobots
{
    /** @var boolean Разрешает ли правило URL. */
    protected $allows;

    /** @var string Маска. */
    protected $mask;

    /**
     * Конструктор.
     *
     * @param string  $mask   Маска. Если пуста - принимается равной "/".
     * @param boolean $allows Разрешает ли правило URL.
     */
    public function __construct(string $mask, bool $allows)
    {
        $mask = urldecode($mask);
        if (empty($mask)) {
            $mask = $allows ? '/' : '';
        }
        $this->mask   = $mask;
        $this->allows = $allows;
    }

    /**
     * Приведение к строке.
     *
     * @return string
     */
    public function __toString(): string
    {
        $command = $this->allows ? 'Allow' : 'Disallow';
        return trim("{$command}: {$this->mask}");
    }

    /**
     * Маска.
     *
     * @return string
     */
    public function getMask(): string
    {
        return $this->mask;
    }

    /**
     * Разрешает ли правило URL.
     *
     * @return boolean
     */
    public function getAllows(): bool
    {
        return $this->allows;
    }

    /**
     * Использует ли маска шаблонные символы.
     *
     * @return boolean
     */
    public function hasRegexp(): bool
    {
        static $regexps = [];

        if (!isset($regexps[$this->mask])) {
            $regexps[$this->mask] = (bool) preg_match('#[\*\$]#u', $this->mask);
        }

        return $regexps[$this->mask];
    }

    /**
     * Получение регулярного выражения по маске.
     *
     * @return string|NULL
     */
    public function getRegexp()
    {
        static $replace = ['*' => '.*', '?' => '\\?', '.' => '\\.'];
        static $regexps = [];

        if ($this->hasRegexp() && empty($regexps[$this->mask])) {
            $regexp = strtr($this->mask, $replace);
            $regexp = preg_replace('#\$.+#', '$', $regexp);
            if ($regexp{0} === '/') {
                $regexp = "^{$regexp}";
            }
            $regexps[$this->mask] = "#{$regexp}#su";
        }

        return ($regexps[$this->mask] ?? null);
    }

    /**
     * Проверка URL на соответствие маске правила.
     *
     * @param string $url URL (путь + GET-запрос + якорь, не валидируется).
     *
     * @return \TextMedia\RobotsTxtParser\RobotsRule|NULL
     */
    public function getMatch(string $url)
    {
        $result = false;
        if (!empty($this->mask)) {
            $url = $this->prepareUrl($url);
            if ($this->hasRegexp()) {
                $result = (bool) preg_match($this->getRegexp(), $url);
            } elseif ($this->mask{0} !== '/') {
                $result = (mb_strpos($url, $this->mask) !== false);
            } else {
                $result = (mb_strpos($url, $this->mask) === 0);
            }
        }
        return ($result ? $this : null);
    }

    /**
     * Сравнение двух правил (для сротировки).
     *
     * @see https://yandex.ru/support/webmaster/controlling-robot/robots-txt.html
     *
     * @param self $x Правило X.
     * @param self $y Правило Y.
     *
     * @return integer
     */
    public static function compare(self $x, self $y): int
    {
        // Чем меньше длина строки, тем выше приоритет проверки.
        $length = (mb_strlen($x->getMask()) <=> mb_strlen($y->getMask()));
        if (0 !== $length) {
            return $length;
        }

        // При прочих равных приоритет отдается директиве Allow.
        $allows = ((int) $x->getAllows() <=> (int) $y->getAllows());
        if (0 !== $allows) {
            return $allows;
        }

        // Во всех прочих случаях - сохраняем исходный порядок.
        return 0;
    }
}
