<?php

namespace TextMedia\RobotsTxtParser;

/**
 * Парсер robots.txt.
 */
class RobotsTxt extends AbstractRobots
{
    /** @var \TextMedia\RobotsTxtParser\RobotsBlock[] Блоки robots.txt ("агент_в_нижнем_регистре" => "блок"). */
    protected $blocks;

    /** @var string[] Ссылки на sitemap-ы. */
    protected $sitemaps;

    /** @var string Хост. */
    protected $host;

    /**
     * Конструктор.
     *
     * @param string                                   $robots Содержимое файла robots.txt.
     * @param \TextMedia\RobotsTxtParser\RobotsAgent[] $agents OPTIONAL Фильтр агентов.
     */
    public function __construct(string $robots, array $agents = [])
    {
        $agents = array_fill_keys(array_map(function (RobotsAgent $agent): string {
            return $agent->getName(true);
        }, $agents), true);

        $this->blocks = $this->sitemaps = [];
        $agent = $block = null;
        foreach (explode("\n", $robots) as $line) {
            $line = trim(preg_replace('/\s+|#.*$/su', '', $line));
            if (mb_strlen($line) === 0 || mb_strpos($line, ':') === false) {
                continue;
            }

            list($key, $val) = explode(':', $line, 2);
            $key = strtolower($key);
            switch ($key) {
                case 'sitemap':
                    $val = urldecode($val);
                    if (!in_array($val, $this->sitemaps)) {
                        array_push($this->sitemaps, $val);
                    }
                    break;
                case 'user-agent':
                    $agent = new RobotsAgent($val);
                    $block = $agent->getName(true);
                    if (empty($this->blocks[$block]) && (empty($agents) || !empty($agents[$block]))) {
                        $this->blocks[$block] = ['agent' => $agent, 'rules' => []];
                    }
                    break;
                case 'allow':
                case 'disallow':
                    if (!empty($block) && !empty($this->blocks[$block])) {
                        $allows = ($key === 'allow');
                        if (!$allows || !empty($val)) {
                            array_push($this->blocks[$block]['rules'], new RobotsRule($val, $allows));
                        }
                    }
                    break;
                case 'host':
                    $this->host = $val;
                    break;
            }
        }

        $this->blocks = array_map(function (array $block): RobotsBlock {
            return new RobotsBlock($block['agent'], $block['rules']);
        }, $this->blocks);
    }

    /**
     * Приведение к строке.
     *
     * @return string
     */
    public function __toString(): string
    {
        return trim(implode("\n\n", $this->blocks) . "\n\n"
            . ($this->host ? "Host: {$this->host}\n" : '')
            . implode("\n", array_map(function (string $sitemap): string {
                return "Sitemap: {$sitemap}";
            }, $this->sitemaps)));
    }

    /**
     * Блоки robots.txt
     *
     * @return \TextMedia\RobotsTxtParser\RobotsBlock[]
     */
    public function getBlocks(): array
    {
        return $this->blocks;
    }

    /**
     * Все агенты robots.txt.
     *
     * @return \TextMedia\RobotsTxtParser\RobotsAgent[]
     */
    public function getAgents(): array
    {
        return array_map(function (RobotsBlock $block): RobotsAgent {
            return $block->getAgent();
        }, $this->blocks);
    }

    /**
     * Блок по указанному агенту.
     *
     * @param \TextMedia\RobotsTxtParser\RobotsAgent $agent Агент.
     *
     * @return \TextMedia\RobotsTxtParser\RobotsBlock|NULL
     */
    public function getBlock(RobotsAgent $agent)
    {
        return ($this->blocks[$agent->getName(true)] ?? null);
    }

    /**
     * Ссылки на sitemap-ы.
     *
     * @return string[]
     */
    public function getSitemaps(): array
    {
        return $this->sitemaps;
    }

    /**
     * Поиск соответсвия.
     *
     * @param string                                   $url        URL (путь + GET-запрос + якорь, не валидируется).
     * @param \TextMedia\RobotsTxtParser\RobotsAgent[] $agents     OPTIONAL Фильтр и порядок агентов.
     * @param boolean                                  $firstAgent OPTIONAL Завершать поиск при первом совпадении (т.е. не проверять все блоки).
     *                                                             По умолчанию равно TRUE, обзор блоков выполняется до первого совпадения.
     * @param boolean                                  $firstRule  OPTIONAL В спике правил выбирать первое совпавшее (т.е. не проверять все правила).
     *                                                             По умолчанию равно FALSE, т.е. актуальным считается последнне совпавшее правило.
     *
     * @return \TextMedia\RobotsTxtParser\RobotsRule|NULL
     */
    public function getMatch(string $url, array $agents = [], bool $firstAgent = true, bool $firstRule = false)
    {
        if (empty($agents)) {
            $agents = $this->getAgents();
        } else {
            array_walk($agents, function (RobotsAgent $agent): RobotsAgent {
                return $agent;
            });
        }

        $result = null;

        $url = $this->prepareUrl($url);
        foreach ($agents as $agent) {
            $block = $this->getBlock($agent);
            if (!empty($block)) {
                $result = $block->getMatch($url, $firstRule);
                if (!empty($result) && $firstAgent) {
                    break;
                }
            }
        }

        return $result;
    }
}
