<?php

namespace TextMedia\RobotsTxtParser;

/**
 * Блок robots.txt.
 */
class RobotsBlock extends AbstractRobots
{
    /** @var \TextMedia\RobotsTxtParser\RobotsAgent Агент. */
    protected $agent;

    /** @var \TextMedia\RobotsTxtParser\RobotsRule[] Правила. */
    protected $rules;

    /**
     * Конструктор.
     *
     * @param \TextMedia\RobotsTxtParser\RobotsAgent  $agent Агент.
     * @param \TextMedia\RobotsTxtParser\RobotsRule[] $rules Правила.
     */
    public function __construct(RobotsAgent $agent, array $rules)
    {
        $this->agent = $agent;
        $this->rules = array_map(function (RobotsRule $rule): RobotsRule {
            return $rule;
        }, $rules);
    }

    /**
     * Приведение к строке.
     *
     * @return string
     */
    public function __toString(): string
    {
        return trim("{$this->agent}\n" . implode("\n", $this->getRules()));
    }

    /**
     * Агент.
     *
     * @return \TextMedia\RobotsTxtParser\RobotsAgent
     */
    public function getAgent(): RobotsAgent
    {
        return $this->agent;
    }

    /**
     * Правила.
     *
     * @return \TextMedia\RobotsTxtParser\RobotsRule[]
     */
    public function getRules(): array
    {
        static $sorted = [];

        $uid = spl_object_hash($this);
        if (empty($sorted[$uid])) { // Подготовка списка правил (сортировка).
            usort($this->rules, [RobotsRule::class, 'compare']);
            $sorted[$uid] = true;
        }

        return $this->rules;
    }

    /**
     * Проверка указанного URL на соответствие правилам.
     *
     * @param string  $url   URL (путь + GET-запрос + якорь, не валидируется).
     * @param boolean $first OPTIONAL В спике правил выбирать первое совпавшее (т.е. не проверять все правила).
     *                       По умолчанию равно FALSE, т.е. актуальным считается последнне совпавшее правило.
     *
     * @return \TextMedia\RobotsTxtParser\RobotsRule|NULL
     */
    public function getMatch(string $url, bool $first = false)
    {
        $last = count($this->getRules()) - 1;
        if ($last >= 0) {
            $url = $this->prepareUrl($url);
            // Если указано, что нужно проверить совпадение со всеми правилами, а не только первым,
            // ищем совпадение все равно только с первым, но по массиву идем из конца в начало,
            // т.е. фактически ищем сразу последнее совпадение.
            for ($from = !$first * $last, $till = $last - $from, $step = $till <=> $from;; $from += $step) {
                $rule = $this->rules[$from];
                if ($rule->getMatch($url)) {
                    return $rule;
                } elseif ($from === $till) {
                    break;
                }
            }
        }
        return null;
    }
}
