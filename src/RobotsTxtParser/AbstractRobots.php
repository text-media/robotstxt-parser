<?php

namespace TextMedia\RobotsTxtParser;

/**
 * Абстракция с общими методами для парсера.
 */
abstract class AbstractRobots
{
    /**
     * Поиск/проверка соответсвия.
     * Потомки могут иметь дополнительные необязательные аргументы.
     *
     * @param string $url URL (путь + GET-запрос + якорь, не валидируется).
     *
     * @return \TextMedia\RobotsTxtParser\RobotsRule|NULL
     */
    abstract public function getMatch(string $url);

    /**
     * Проверка, разрешает ли данный robots.txt/блок/правило указанный URL.
     * Аргументы - те же, что и у метода getMatch() класса.
     *
     * @return boolean
     */
    public function allows(): bool
    {
        $match = call_user_func_array([$this, 'getMatch'], func_get_args());
        return (!$match || $match->getAllows());
    }

    /**
     * Проверка, запрещает ли данный robots.txt/блок/правило указанный URL.
     * Аргументы - те же, что и у метода getMatch() класса.
     *
     * @return boolean
     */
    public function disallows(): bool
    {
        $match = call_user_func_array([$this, 'getMatch'], func_get_args());
        return ($match && !$match->getAllows());
    }

    /**
     * Подготовка URL к сравнений с правилом.
     * Из URL выпиливается протокол, хост, порт и проч.,
     * оставляются только путь, GET-запрос и якорь.
     *
     * @param string $url Исходный URL (не валидируется).
     *
     * @return string
     */
    protected function prepareUrl(string $url): string
    {
        static $prepared = [];

        if (empty($prepared[$url])) {
            $url = preg_replace('#^(https?:)?//[^/]+#iu', '', $url);
            $prepared[$url] = urldecode($url ?: '/');
        }

        return $prepared[$url];
    }
}
