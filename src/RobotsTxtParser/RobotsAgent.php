<?php

namespace TextMedia\RobotsTxtParser;

/**
 * Агент robots.txt.
 */
class RobotsAgent
{
    /** @var string Имя агента. */
    protected $name;

    /**
     * Конструктор.
     *
     * @param string $name OPTIONAL Имя агента (если пусто = "*").
     */
    public function __construct(string $name = '*')
    {
        $this->name = $name ?: '*';
    }

    /**
     * Приведение к строке.
     *
     * @return string
     */
    public function __toString(): string
    {
        return "User-agent: {$this->name}";
    }

    /**
     * Имя агента.
     *
     * @param boolean $sensitive OPTIONAL Должно ли быть отдано в нечусыительной к регистру форме.
     *                           По умолчанию = FALSE; иначе - приводится к нижнему регистру.
     *
     * @return string
     */
    public function getName(bool $sensitive = false): string
    {
        return $sensitive ? mb_strtolower($this->name) : $this->name;
    }
}
